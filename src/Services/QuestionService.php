<?php

namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Session;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__.'/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    private array $randomQuestions;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    private function getRandomQuestions(int $count = 5): string
    {
        //TODO: Get {$count} random questions from JSON
        $questions = $this->json->decode(file_get_contents(self::QUESTIONS_PATH));
        shuffle($questions);

        return $this->json->encode(array_slice($questions, 0, $count));
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        //TODO: Calculate points for the answer given
    }

    public function getNextQuestion()
    {
        Session::set('questionCount', count($this->getAnsweredUsed()));

        return $this->jsonMapper->map($this->getRandomSessionQuestions()[Session::get('questionCount')], new Question());
    }

    private function getRandomSessionQuestions(): array
    {
        return $this->json->decode(Session::get('randomQuestions'));
    }

    public function initQuestions($name)
    {
        Session::set('name', $name);
        Session::set('questionCount', 0);
        Session::set('randomQuestions', $this->getRandomQuestions(5));
        Session::set('actualScore', 0);
        Session::set('answerUsed', '');
    }

    public function getQuestionId(int $id)
    {
        $question = array_filter(
            $this->getRandomSessionQuestions(), function ($q) use ($id) {
            return ($q->id === intval($id));
        }
        );

        return array_values($question)[0] ?? null;
    }

    public function getAnswerScore(object $question, string $answer)
    {
        $answerValue = ($this->isAnswerUsed($question->id)) ? 0 : $question->points;
        $this->setAnswerUse($question->id);

        return ($question->correctAnswer === $answer) ? $answerValue : null;
    }

    private function setAnswerUse($id)
    {
        Session::set('answerUsed', implode(',', array_filter(array_unique(array_merge($this->getAnsweredUsed(), [$id])))));
    }

    private function getAnsweredUsed(): array
    {
        return explode(',', Session::get('answerUsed'));
    }

    private function isAnswerUsed($id)
    {
        return in_array($id, $this->getAnsweredUsed());
    }

}