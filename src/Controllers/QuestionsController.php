<?php

namespace Ucc\Controllers;

use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        $this->questionService->initQuestions($name);
        //TODO Get first question for user
        $question = $this->questionService->getNextQuestion();

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if (Session::get('name') === null) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();

            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        if (empty($question = $this->questionService->getQuestionId($id))) {
            return $this->json(['message' => "The question id $id you are trying to use is invalid."], 400);;
        }
        $answerScore = $this->questionService->getAnswerScore($question, $answer);
        $message = (null !== $answerScore) ? 'Correct!' : 'Ups, wrong answer!';
        $actualScore = $answerScore ?? 0;
        Session::set('actualScore', Session::get('actualScore') + $actualScore);
        $question = $this->questionService->getNextQuestion();

        return $this->json(['message' => $message, 'question' => $question, 'actualScore' => Session::get('actualScore')]);
    }
}