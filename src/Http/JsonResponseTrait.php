<?php

namespace Ucc\Http;

use Ucc\Session;

trait JsonResponseTrait
{
    public function json($data, int $statusCode = 200): bool
    {
        http_response_code($statusCode);
        header('Session-Id: '.session_id());
        header('Content-Type: application/json;charset=utf-8');
        if (true) {
            $session = [
                'session_debug' => [
                    'name' => Session::get('name'),
                    'actualScore' => Session::get('actualScore'),
                    'questionCount' => Session::get('questionCount'),
                    'answerUsed' => Session::get('answerUsed'),
                    'randomQuestions' => array_map(function($q){
                        return $q->id;
                    },
                    json_decode(Session::get('randomQuestions'))),
                ],
            ];
            $data = array_merge($data, $session);
        }
        $body = json_encode($data);
        if (json_last_error() === JSON_ERROR_NONE) {
            echo $body;
        }

        return "Ups - something went wrong, sorry.";
    }
}